#!/bin/bash

if [[ $EUID -ne 0 ]]; then
  echo [err] this script must be run as the root user
  exit 1
fi

EXTERNAL_ROOT="/media/external"
EXTERNAL_SERIES_ROOT="$EXTERNAL_ROOT/series"

if [[ -z "$2" ]]; then
  echo [err] incorrect arguments passed
  echo usage: "media-sync.sh TARGET_UUID SOURCES_LIST"
  echo "  TARGET_UUID  : UUID of target device"
  echo "  SOURCES_LIST : text file with list of series"
  exit 1
fi

if [[ ! -r "$2" ]]; then
  echo [err] specified sources list not accessible
  echo [inf] tried to load: "$2"
  exit 1
else
  TARGET_UUID="$1"
  SOURCES_LIST="$2"
fi

if [[ -d "$EXTERNAL_SERIES_ROOT" ]]; then
  echo [inf] external already mounted
  AUTOMOUNT=0
else
  echo [inf] attempting to mount external...
  mount -t ntfs-3g --uuid "$TARGET_UUID" /media/external

  if [[ -d "$EXTERNAL_SERIES_ROOT" ]]; then
    echo [inf] external successfully mounted
    AUTOMOUNT=1
  else
    echo [err] external failed to mount
    exit 1
  fi
fi

echo [inf] starting series sync...
cat "$SOURCES_LIST" | while read SERIES_LIST
do
  echo [inf] processing: "$SERIES_LIST"...

  SOURCE_ROOT=
  SYNC_SRC=
  SYNC_DEST=

  if [[ -d "/media/media1/series-family/$SERIES_LIST" ]]; then
    SOURCE_ROOT="/media/media1/series-family"
  elif [[ -d "/media/media1/series-mature/$SERIES_LIST" ]]; then
    SOURCE_ROOT="/media/media1/series-mature"
  elif [[ -d "/media/media2/series-family/$SERIES_LIST" ]]; then
    SOURCE_ROOT="/media/media2/series-family"
  elif [[ -d "/media/media2/series-mature/$SERIES_LIST" ]]; then
    SOURCE_ROOT="/media/media2/series-mature"
  elif [[ -d "/media/media2/series-others/$SERIES_LIST" ]]; then
    SOURCE_ROOT="/media/media2/series-others"
  fi

  SEASON=$(ls -vr "$SOURCE_ROOT/$SERIES_LIST" | grep "Season" | head -1)
  SYNC_SRC="$SOURCE_ROOT/$SERIES_LIST/$SEASON/"
  SYNC_DEST="$EXTERNAL_SERIES_ROOT/$SERIES_LIST/$SEASON/"

  if [[ -d "$SYNC_SRC" ]]; then
    if [[ -d $EXTERNAL_SERIES_ROOT && ! -d "$SYNC_DEST" ]]; then
      mkdir -p "$SYNC_DEST"
    fi
    rsync -ruq "$SYNC_SRC" "$SYNC_DEST"
  else
    echo [inf] source doesn\'t exist - skipping sync for: "$SYNC_SRC"
  fi
done

if [[ $AUTOMOUNT -eq 1 ]]; then
  echo [inf] external was mounted automatically, unmounting now...
  umount "$EXTERNAL_ROOT"
else
  echo [inf] external was not mounted automatically, leaving it alone
fi
